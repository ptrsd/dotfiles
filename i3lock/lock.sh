#!/usr/bin/env bash

TMPBG=/tmp/screen.png
scrot $TMPBG && convert $TMPBG -scale 5% -scale 2000% $TMPBG

i3lock \
	-t -i $TMPBG \
	--ringcolor=c5c8c644 \
	--linecolor=ffffff44 \
	--separatorcolor=1d1f2144 \
    	--keyhlcolor=c5c8c6ff \
	--bshlcolor=cc342b88 \
    	--insidecolor=1d1f21aa \
	--line-uses-inside \
	--insidevercolor=1d1f21aa \
    	--ringvercolor=3971ed44 \
	--ringwrongcolor=cc342baa \
	--insidewrongcolor=1d1f21aa \
    	--radius=30 \
	--wrongtext="" \
        --veriftext=""

rm $TMPBG
